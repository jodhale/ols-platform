﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class SellerActor : MonoBehaviour {

	const float maxSize = 236;
	[SerializeField] GameObject panelObject;
	[SerializeField] GameObject editObject;

	List<GameObject> buttons = new List<GameObject>();
	List<string> userKey = new List<string>();

	// Use this for initialization
	void Start () {
		for (int i = 0; i < panelObject.transform.childCount; i++) {
			buttons.Add (panelObject.transform.GetChild (i).gameObject);
			buttons [i].SetActive (false);
		}

		Refresh ();
	}
	
	public void Refresh(){
		print (KeyHolder.Instance.BuyerID);
		print (KeyHolder.Instance.SellerID);
		//print (PlayerPrefs.GetInt(Seller.STAGE + PlayerPrefs.GetString (Customer.USER + 1) + PlayerPrefs.GetString (Seller.USER + 1)));

		for(int i = 0; PlayerPrefs.GetString(Customer.USER + i) != ""; i++){
			print ("nan");

			if (PlayerPrefs.GetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + i) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID)) != 0) {
				userKey.Add (Customer.USER + i);
				print (i);
			}
		}

		for(int i = 0; i < userKey.Count; i++){
			buttons [i].SetActive (true);
			panelObject.GetComponent<RectTransform>().sizeDelta += new Vector2(0, maxSize);
			buttons[i].GetComponent<BuyerBarActor>().ChangeValues(PlayerPrefs.GetString(Customer.NAME + i), PlayerPrefs.GetString(Customer.USER + i));
		}
	}

	public void EditInfo(){
		editObject.SetActive (true);
	}
}
