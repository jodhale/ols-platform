﻿using UnityEngine.UI;
using UnityEngine;
using OLSEditor;

public class KeyHolder : MonoBehaviour{

	public static KeyHolder Instance;
	public int BuyerID{get;set;}
	public int SellerID{get;set;}

	[Header("Message")]
	[SerializeField] GameObject messageObject;
	[SerializeField] Text messageText;

	void Awake(){
		DontDestroyOnLoad(gameObject);
		Instance = this;
		Screen.SetResolution (480, 854, false);
		messageObject.SetActive (false);
		Scenes.Change (Scenes.LOGIN);
	}

	public void ShowMessage(string message){
		messageText.text = message;
		messageObject.SetActive(true);
	}

	public void HideMessage(){
		messageObject.SetActive(false);
		PlayerPrefs.SetInt ("NIBS", 0);
	}
}