﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class ViewSeller : MonoBehaviour {

	public static ViewSeller Instance;
	[SerializeField] Text serviceText;
	[SerializeField] Text nameText;
	[SerializeField] Text starText;
	[SerializeField] Text priceText;
	[SerializeField] Text descText;
	[SerializeField] GameObject chatBox;
	[SerializeField] ReviewActor reviewBox;
	[SerializeField] GameObject rateBox;
	[SerializeField] GameObject firstBox;
	[SerializeField] GameObject secondBox;
	[SerializeField] GameObject thirdBox;
	[SerializeField] GameObject fourthBox;
	[SerializeField] Text reviewText;

	// Use this for initialization
	void Start () {
		Instance = this;
		gameObject.SetActive (false);
	}

	public void View(int i){
		KeyHolder.Instance.SellerID = i;

		gameObject.SetActive (true);
		serviceText.text = PlayerPrefs.GetString (Seller.SELL + i);
		nameText.text = PlayerPrefs.GetString (Seller.NAME + i);

		float star = Seller.Rate(i);
		if(star != 0)
			starText.text = Seller.Rate(i) + "";

		reviewText.text = "CHECK (" + PlayerPrefs.GetFloat (Seller.BUYER_NUM + i) + ") REVIEWS";
		priceText.text = "$" + PlayerPrefs.GetInt (Seller.MIN + i) + "-" + PlayerPrefs.GetInt (Seller.MAX + i);
		descText.text = "Address: " + PlayerPrefs.GetString (Seller.ADRS + i) + "\nCity: " + PlayerPrefs.GetString(Seller.CITY + i) + "\nCompany: " + PlayerPrefs.GetString(Seller.COMP + i) + "\n" + PlayerPrefs.GetString (Seller.DISC + i);

		switch(PlayerPrefs.GetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + i) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID))){
		case 1:
			firstBox.SetActive (false);
			secondBox.SetActive (true);
			thirdBox.SetActive (false);
			fourthBox.SetActive (false);
			break;
		case 2:
			firstBox.SetActive (false);
			secondBox.SetActive (false);
			thirdBox.SetActive (true);
			fourthBox.SetActive (false);
			break;
		case 3:
			firstBox.SetActive (false);
			secondBox.SetActive (false);
			thirdBox.SetActive (false);
			fourthBox.SetActive (true);
			break;
		default:
			firstBox.SetActive (true);
			secondBox.SetActive (false);
			thirdBox.SetActive (false);
			fourthBox.SetActive (false);
			break;
		}
	}

	public void Back(){
		gameObject.SetActive (false);
	}

	public void Chat(){
		chatBox.SetActive (true);
	}

	public void Review(){
		reviewBox.OpenReviews ();
	}

	public void FirstStage(){
		PlayerPrefs.SetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID), 1);
		firstBox.SetActive (false);
		secondBox.SetActive (false);
		thirdBox.SetActive (true);
		fourthBox.SetActive (false);
	}

	public void CancelOrder(){
		PlayerPrefs.SetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID), 0);
		firstBox.SetActive (true);
		secondBox.SetActive (false);
		thirdBox.SetActive (false);
		fourthBox.SetActive (false);
	}

	public void Rate(){
		rateBox.SetActive (true);
	}
}
