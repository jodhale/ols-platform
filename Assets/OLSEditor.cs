﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace OLSEditor{

	public static class Customer{
		public const string NAME = "CName";
		public const string USER = "CUser";
		public const string PASS = "CPass";
		public const string MAIL = "CMail";
		public const string ADRS = "CAdrs";
		public const string CITY = "CCity";
	}

	public static class Seller{
		public const string NAME = "SName";
		public const string USER = "SUser";
		public const string PASS = "SPass";
		public const string MAIL = "SMail";
		public const string ADRS = "SAdrs";
		public const string CITY = "SCity";
		public const string SELL = "SSell";
		public const string STAR = "SStar";
		public const string DISC = "SDisc";
		public const string REVW = "SRevw";
		public const string COMP = "SComp";
		public const string BUYER_NUM = "BNum";
		public const string MAX = "Max";
		public const string MIN = "Min";
		public const string STAGE = "Stage";


		public static float Rate(int i){
			return PlayerPrefs.GetFloat (STAR + i) / PlayerPrefs.GetFloat (BUYER_NUM + i);
		}
	}

	public static class Scenes{
		public const string LOGIN = "LogIn";
		public const string REGISTER = "Register";
		public const string BUYER = "Buyer";
		public const string SELLER = "Seller";

		public static void Change(string scene){
			SceneManager.LoadScene (scene);
		}
	}

	public static class Prefs{
		public static bool GETBOOL(string pref){
			if (PlayerPrefs.GetInt (pref) != 0)
				return true;
			else
				return false;
		}

		public static void SETBOOL(string pref, bool val){
			if (val)
				PlayerPrefs.SetInt (pref, 1);
			else
				PlayerPrefs.SetInt (pref, 0);
		}
	}
}