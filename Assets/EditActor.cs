﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class EditActor : MonoBehaviour {

	[SerializeField] InputField companyText;
	[SerializeField] Text serviceText;
	[SerializeField] InputField descText;
	[SerializeField] InputField minVal;
	[SerializeField] InputField maxVal;

	void Start(){
		int i = KeyHolder.Instance.SellerID;

		companyText.text = PlayerPrefs.GetString (Seller.COMP + i);
		serviceText.text = PlayerPrefs.GetString (Seller.SELL + i);
		descText.text = PlayerPrefs.GetString (Seller.DISC + i);
		minVal.text = PlayerPrefs.GetInt (Seller.MIN + i) + "";
		maxVal.text = PlayerPrefs.GetInt (Seller.MAX + i) + "";
	}

	public void UpdateInfo(){
		int i = KeyHolder.Instance.SellerID;

		PlayerPrefs.SetString (Seller.COMP + i, companyText.text);
		PlayerPrefs.SetString (Seller.SELL + i, serviceText.text);
		PlayerPrefs.SetString (Seller.DISC + i, descText.text);
		PlayerPrefs.SetInt (Seller.MIN + i, int.Parse(minVal.text));
		PlayerPrefs.SetInt (Seller.MAX + i, int.Parse(maxVal.text));
	}

	public void Back(){
		gameObject.SetActive (false);
	}
}
