﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class ViewBuyer : MonoBehaviour {

	public static ViewBuyer Instance;
	[SerializeField] Text nameText;
	[SerializeField] Text descText;
	[SerializeField] GameObject secondBox;
	[SerializeField] GameObject thirdBox;
	[SerializeField] GameObject chatBox;

	void Start(){
		Instance = this;
		gameObject.SetActive (false);
	}

	public void View(int i){
		KeyHolder.Instance.BuyerID = i;

		nameText.text = PlayerPrefs.GetString (Customer.NAME + i);
		descText.text = "Address: " + PlayerPrefs.GetString (Customer.ADRS + i);
		gameObject.SetActive (true);

		switch(PlayerPrefs.GetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + i) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID))){
		case 1:
			secondBox.SetActive (true);
			thirdBox.SetActive (false);
			break;
		case 2:
			secondBox.SetActive (false);
			thirdBox.SetActive (true);
			break;
		default:
			secondBox.SetActive (false);
			thirdBox.SetActive (false);
			break;
		}
	}

	public void SecondStage(){
		PlayerPrefs.SetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID), 2);
		secondBox.SetActive (false);
		thirdBox.SetActive (true);
	}

	public void ThirdStage(){
		PlayerPrefs.SetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID), 3);
		secondBox.SetActive (false);
		thirdBox.SetActive (false);
	}

	public void Chat(){
		chatBox.SetActive (true);
	}

	public void Back(){
		gameObject.SetActive (false);
	}
}
