﻿using OLSEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RegistrationActor : MonoBehaviour {

	[SerializeField] GameObject verifyScreen;
	[SerializeField] Text verifyText;
	[SerializeField] Text userText;
	[SerializeField] Text nameText;
	[SerializeField] Text mailText;
	[SerializeField] Text addText;
	[SerializeField] Text cityText;
	[SerializeField] Text pass1;
	[SerializeField] Text pass2;

	int customerIndex;
	int sellerIndex;
	int verificationKey;

	bool isSeller;

	void Start(){
		while(PlayerPrefs.GetString(Customer.USER + customerIndex) != "")
			customerIndex++;

		while(PlayerPrefs.GetString(Seller.USER + sellerIndex) != "")
			sellerIndex++;

		print (sellerIndex);
	}

	public void CustomerRegister(){
		isSeller = false;
		CheckSides ();

	}

	public void SellerRegister(){
		isSeller = true;
		CheckSides ();

	}

	void CheckSides(){
		if(CheckExist(sellerIndex, Seller.USER, userText.text, "Username already exists") || CheckExist(sellerIndex, Seller.MAIL, mailText.text, "E-mail already exists") || CheckPassword())
			return;

		if(CheckExist(customerIndex, Customer.USER, userText.text, "Username already exists") || CheckExist(customerIndex, Customer.MAIL, mailText.text, "E-mail already exists") || CheckPassword())
			return;

		LoadVerification();
	}

	/*
				public void Validate(){
					if(verificationKey == isRight) {
						if (!isSeller) {
				RegisterAccount (Customer.USER + customerIndex, Customer.NAME + customerIndex, Customer.PASS + customerIndex, Customer.ADRS + customerIndex, Customer.CITY + customerIndex, Customer.MAIL + customerIndex);
				Scenes.Change (Scenes.BUYER);
			} else {
				RegisterAccount (Seller.USER + sellerIndex, Seller.NAME + sellerIndex, Seller.PASS + sellerIndex, Seller.ADRS + sellerIndex, Seller.CITY + sellerIndex, Seller.MAIL + sellerIndex);
				Scenes.Change (Scenes.SELLER);
			}
					} else
						print("Wrong Key Entered");
				}*/


	void LoadVerification(){
		verifyScreen.SetActive(true);
		verificationKey = Random.Range(100000, 999999);


		//Delete this later
		if (!isSeller) {
			KeyHolder.Instance.BuyerID = customerIndex;
			RegisterAccount (Customer.USER + customerIndex, Customer.NAME + customerIndex, Customer.PASS + customerIndex, Customer.ADRS + customerIndex, Customer.CITY + customerIndex, Customer.MAIL + customerIndex);
			Scenes.Change (Scenes.BUYER);
		} else {
			KeyHolder.Instance.SellerID = sellerIndex;
			RegisterAccount (Seller.USER + sellerIndex, Seller.NAME + sellerIndex, Seller.PASS + sellerIndex, Seller.ADRS + sellerIndex, Seller.CITY + sellerIndex, Seller.MAIL + sellerIndex);
			Scenes.Change (Scenes.SELLER);
		}

		//Send mail
	}

	void RegisterAccount(string user, string name, string pass, string address, string city, string mail){
		PlayerPrefs.SetString(user, userText.text);
		PlayerPrefs.SetString(name, nameText.text);
		PlayerPrefs.SetString(pass, pass1.text);
		PlayerPrefs.SetString(address, addText.text);
		PlayerPrefs.SetString(city, cityText.text);
		PlayerPrefs.SetString(mail, mailText.text);
	}

	bool CheckExist(int index, string key, string target, string notice){
		for(int i = 0; i < index; i++)	{
			if(PlayerPrefs.GetString(key + i) == target){
				KeyHolder.Instance.ShowMessage (notice);
				return true;
			}
		}

		return false;
	}

	bool CheckPassword(){
		if(pass1.text != pass2.text) {
			KeyHolder.Instance.ShowMessage ("Password does not match");
			return true;
		} else
			return false;		
	}
}