﻿using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class ChatActor : MonoBehaviour {

	[SerializeField] Text chatText;
	[SerializeField] InputField replyText;
	[SerializeField] bool isSeller;

	void Update () {
		chatText.text = PlayerPrefs.GetString ("Chat" + PlayerPrefs.GetString(Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString(Seller.USER + KeyHolder.Instance.SellerID));
	}

	public void Reply(){
		string id = Customer.USER + KeyHolder.Instance.BuyerID;

		if (isSeller)
			id = Seller.USER + KeyHolder.Instance.SellerID;

		PlayerPrefs.SetString ("Chat" + PlayerPrefs.GetString(Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString(Seller.USER + KeyHolder.Instance.SellerID), chatText.text + "\n" + PlayerPrefs.GetString (id) + ": " + replyText.text);
		replyText.text = "";
	}

	public void Back(){
		gameObject.SetActive (false);
	}
}
