﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class ReviewActor : MonoBehaviour {

	[SerializeField] Text reviewText;

	public void OpenReviews(){
		gameObject.SetActive (true);
		reviewText.text = PlayerPrefs.GetString (Seller.REVW + KeyHolder.Instance.SellerID);
	}

	public void CloseReviews(){
		gameObject.SetActive (false);
	}
}
