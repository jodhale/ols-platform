﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class BuyerBarActor : MonoBehaviour {

	[SerializeField] Text nameText;
	string userKey;

	public void ChangeValues(string name, string user){
		userKey = user;
		nameText.text = name;
	}

	public void ViewProfile(){
		int i = 0;

		while(PlayerPrefs.GetString(Customer.USER + i) != userKey)
			i++;

		ViewBuyer.Instance.View (i);
	}
}
