﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class BuyerActor : MonoBehaviour {

	const float maxSize = 236;
	[SerializeField] GameObject panelObject;
	[SerializeField] Text serviceText;
	[SerializeField] Text cityText;
	[SerializeField] Text maxText;
	[SerializeField] Text minText;

	List<GameObject> buttons = new List<GameObject>();
	List<string> userKey = new List<string>();

	void Start(){
		for (int i = 0; i < panelObject.transform.childCount; i++) {
			buttons.Add (panelObject.transform.GetChild (i).gameObject);
			buttons [i].SetActive (false);
		}
	}

	public void Search(){
		foreach (GameObject b in buttons)
			b.SetActive (false);

		userKey.Clear ();

		for (int i = 0; PlayerPrefs.GetString (Seller.SELL + i) != ""; i++) {
			if(PlayerPrefs.GetString(Seller.SELL + i) == serviceText.text){
				if(PlayerPrefs.GetString(Seller.CITY + i) == cityText.text){
					if(PlayerPrefs.GetInt(Seller.MAX + i) <= int.Parse(maxText.text)){
						if(PlayerPrefs.GetInt(Seller.MIN + i) >= int.Parse(minText.text))
							userKey.Add(Seller.USER + i);
					}
				}
			}
		}

		for(int i = 0; i < userKey.Count; i++){
			buttons [i].SetActive (true);
			panelObject.GetComponent<RectTransform>().sizeDelta += new Vector2(0, maxSize);
			buttons[i].GetComponent<SellerBarActor>().ChangeValues(PlayerPrefs.GetInt(Seller.MIN + i) + "-" + PlayerPrefs.GetInt(Seller.MAX + i), Seller.Rate(i), PlayerPrefs.GetString(Seller.NAME + i), PlayerPrefs.GetString(Seller.USER + i));
		}
	}
}
