﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OLSEditor;
using UnityEngine.UI;

public class SellerBarActor : MonoBehaviour {

	[SerializeField] Text nameText;
	[SerializeField] Text starText;
	[SerializeField] Text priceText;

	string userKey;

	public void ChangeValues(string price, float stars, string name, string user){
		userKey = user;
		priceText.text = "$" + price;
		if(stars != 0)
			starText.text = stars + "";
		nameText.text = name;
	}

	public void ViewProfile(){
		int i = 0;

		while(PlayerPrefs.GetString(Seller.USER + i) != userKey)
			i++;

		ViewSeller.Instance.View (i);
	}
}
