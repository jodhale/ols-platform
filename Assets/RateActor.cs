﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OLSEditor;

public class RateActor : MonoBehaviour {

	[SerializeField] Slider score;
	[SerializeField] Text thoughts;

	public void Submit(){
		int i = KeyHolder.Instance.SellerID;

		PlayerPrefs.SetFloat (Seller.BUYER_NUM + i, PlayerPrefs.GetFloat (Seller.BUYER_NUM + i) + 1);
		PlayerPrefs.SetFloat (Seller.STAR + i, PlayerPrefs.GetFloat (Seller.STAR + i) + score.value);
		PlayerPrefs.SetString (Seller.REVW + i, PlayerPrefs.GetFloat (Seller.REVW + i) + "\n" + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + " rates " + score.value + " stars:\n" + thoughts.text);
		PlayerPrefs.SetInt (Seller.STAGE + PlayerPrefs.GetString (Customer.USER + KeyHolder.Instance.BuyerID) + PlayerPrefs.GetString (Seller.USER + KeyHolder.Instance.SellerID), 0);

		gameObject.SetActive (false);
	}
}
