﻿using OLSEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LogInActor : MonoBehaviour {

	[SerializeField] Text userText;
	[SerializeField] Text passText;

	public void LogIn(){
		int i = 0;
		string userName = userText.text;	//the string of the username
		string password = passText.text;	//the string of the password
		string passwordKey = "";

		while(true){
			if(PlayerPrefs.GetString(Customer.USER + i) == userName){
				passwordKey = Customer.PASS + i;
				break;
			}

			if(PlayerPrefs.GetString(Seller.USER + i) == userName){
				passwordKey = Seller.PASS + i;
				break;
			}

			if(PlayerPrefs.GetString(Customer.USER + i) == "" && PlayerPrefs.GetString(Seller.USER + i) == ""){
				KeyHolder.Instance.ShowMessage ("Username not found");
				return;
			}

			i++;
		}

		if (PlayerPrefs.GetString (passwordKey) == password) {

			if (PlayerPrefs.GetString (Customer.USER + i) == userName) {
				KeyHolder.Instance.BuyerID = i;
				Scenes.Change (Scenes.BUYER);
			} else {
				KeyHolder.Instance.SellerID = i;
				Scenes.Change (Scenes.SELLER);
			}
		} else
			KeyHolder.Instance.ShowMessage ("Wrong Password");
	}

	public void Register(){
		Scenes.Change (Scenes.REGISTER);
	}
}